# README #

## How to Use

* Clone Repository
* Run ```npm install```
* Update fields in data.json
* Run ```npm start```
* App will start on PORT 3000 by default


## License

MIT License allows for commercial use, modifications, distribution, public and private use.